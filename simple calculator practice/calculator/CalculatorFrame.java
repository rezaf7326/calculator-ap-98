import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;


public class CalculatorFrame extends JFrame
{  
   private JTextField display;
   boolean calculationFinished = false;

   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 300;

   public CalculatorFrame()
   {
      display = new JTextField("");
      display.setEditable(false);           
      add(display, BorderLayout.NORTH);
      createButtonPanel();
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }
      
   /**
      Creates the button panel.
   */
   private void createButtonPanel()
   {      
      JPanel buttonPanel = new JPanel();
      buttonPanel.setLayout(new GridLayout(4, 4));

      buttonPanel.add(makeDigitButton("7"));
      buttonPanel.add(makeDigitButton("8"));
      buttonPanel.add(makeDigitButton("9"));
      buttonPanel.add(makeOperatorButton("/"));
      buttonPanel.add(makeDigitButton("4"));
      buttonPanel.add(makeDigitButton("5"));
      buttonPanel.add(makeDigitButton("6"));
      buttonPanel.add(makeOperatorButton("*"));
      buttonPanel.add(makeDigitButton("1"));
      buttonPanel.add(makeDigitButton("2"));
      buttonPanel.add(makeDigitButton("3"));
      buttonPanel.add(makeOperatorButton("-"));
      buttonPanel.add(makeDigitButton("0"));
      buttonPanel.add(makeDigitButton("."));
      buttonPanel.add(makeOperatorButton("="));
      buttonPanel.add(makeOperatorButton("+"));

      add(buttonPanel, BorderLayout.CENTER);
   }

   class DigitButtonListener implements ActionListener
   {
      private String digit;
      public DigitButtonListener(String aDigit)
      {
         digit = aDigit;
      }
      public void actionPerformed(ActionEvent event)
      {
          if (calculationFinished) {
              display.setText(digit);
              calculationFinished = false;
          }
          else
                display.setText(display.getText() + digit);
      }
   }

   class OperatorButtonListener implements ActionListener {

       private String operator;
       public OperatorButtonListener(String aOperator) { operator = aOperator; }

       public void actionPerformed(ActionEvent event){
          display.setText(display.getText() + operator);

          if(operator.equals("=") && !calculationFinished) {
             display.setText(result(display.getText().substring(0, display.getText().length()-1)));
             calculationFinished = true;
          }else if(operator.equals("=") && calculationFinished) {
              display.setText(display.getText().substring(0, display.getText().length() - 1));
          }
          if(!operator.equals("=") && calculationFinished)
              calculationFinished = false;
      }

      String result (String input) {

         int opIndex = 0;
         while(input.indexOf("*", opIndex) != -1) {

                opIndex = input.indexOf("*", opIndex);

               String right = getNumAtTheRight(input, opIndex);
               String left = getNumAtTheLeft(input, opIndex);

               double rightSide;
               if (right.equals("")) rightSide = 1;
               else rightSide = Double.parseDouble(right);

               double leftSide;
               if(left.equals("")) leftSide = 1;
               else leftSide = Double.parseDouble(left);

               double subResult = leftSide * rightSide;

               int replaceRightEdge = opIndex + right.length() + 1;
               int replaceLeftEdge = opIndex - left.length();

               input = input.substring(0, replaceLeftEdge) + subResult + input.substring(replaceRightEdge);
         }


          opIndex = 0;
         while(input.indexOf("/", opIndex) != -1) {

                opIndex = input.indexOf("/", opIndex);

               String right = getNumAtTheRight(input, opIndex);
               String left = getNumAtTheLeft(input, opIndex);

               double rightSide;
               if (right.equals("")) rightSide = 1;
               else rightSide = Double.parseDouble(right);

               double leftSide;
               if(left.equals("")) leftSide = 1;
               else leftSide = Double.parseDouble(left);

               double subResult = leftSide / rightSide;

               int replaceRightEdge = opIndex + right.length() + 1;
               int replaceLeftEdge = opIndex - left.length();

               input = input.substring(0, replaceLeftEdge) + subResult + input.substring(replaceRightEdge);
         }


          opIndex = 0;
         while(input.indexOf("+", opIndex) != -1) {

                opIndex = input.indexOf("+", opIndex);

               String right = getNumAtTheRight(input, opIndex);
               String left = getNumAtTheLeft(input, opIndex);


               double rightSide;
               if (right.equals("")) rightSide = 0;
               else rightSide = Double.parseDouble(right);

               double leftSide;
               if(left.equals("")) leftSide = 0;
               else leftSide = Double.parseDouble(left);

               double subResult = leftSide + rightSide;

               int replaceRightEdge = opIndex + right.length() + 1;
               int replaceLeftEdge = opIndex - left.length();

               input = input.substring(0, replaceLeftEdge) + subResult + input.substring(replaceRightEdge);

         }


          opIndex = 0;
         while(input.indexOf("-", opIndex) != -1) {

                opIndex = input.indexOf("-", opIndex);

               String right = getNumAtTheRight(input, opIndex);
               String left = getNumAtTheLeft(input, opIndex);

               double rightSide;
               if (right.equals("")) rightSide = 0;
               else rightSide = Double.parseDouble(right);

               double leftSide;
               if(left.equals("")) leftSide = 0;
               else leftSide = Double.parseDouble(left);

               double subResult = leftSide - rightSide;

               int replaceRightEdge = opIndex + right.length() + 1;
               int replaceLeftEdge = opIndex - left.length();

               input = input.substring(0, replaceLeftEdge) + subResult + input.substring(replaceRightEdge);
         }

         return input;
      }

      String getNumAtTheLeft(String inputText, int opIndex) {

         for(int index = opIndex - 1; index >= 0; index--){
            if(!"1234567890.".contains(inputText.charAt(index) + "")) {
                if (index == opIndex - 1)
                    throw new RuntimeException("wrong math ops!");
                return inputText.substring(index + 1, opIndex);
            }
         }
         return inputText.substring(0, opIndex);
      }

      String getNumAtTheRight(String inputText, int opIndex) {

         for(int index = opIndex + 1; index < inputText.length(); index++){
             if(!"1234567890.".contains(inputText.charAt(index) + "")) {
                 if (index == opIndex + 1)
                     throw new RuntimeException("wrong math ops!");
                 return inputText.substring(opIndex + 1, index);
             }
         }
         return inputText.substring(opIndex + 1);
      }

   }


   public JButton makeDigitButton(String digit)
   {
      JButton button = new JButton(digit);
      
      ActionListener listener = new DigitButtonListener(digit);
      button.addActionListener(listener);  
      return button;    
   } 


   public JButton makeOperatorButton(String op)
   {
      JButton button = new JButton(op);

      ActionListener listener = new OperatorButtonListener(op);
      button.addActionListener(listener);
      return button;    
   }     
}
